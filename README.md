# RetroShield for Arduino Mega

![RetroShield in Action](./docs/images/RetroShield6502_in_action.jpg)

I'm excited to announce a very cool project for retromaniacs:

* RetroShield is an 8-bit microprocessor daughtercard for Arduino Mega.
  * A real 8-bit microprocessor executes programs while Arduino Mega emulates RAM, ROM, and basic peripherals.
  * Support for Teensy3.5 is in the works.
* Hardware/Arduino Software for 6 microprocessors released: 
  * **MOS**: **[6502](https://en.wikipedia.org/wiki/MOS_Technology_6502)**
  * **Motorola**: **[6809](https://en.wikipedia.org/wiki/Motorola_6809)**
  * **Intel**: **[8031](https://en.wikipedia.org/wiki/Intel_MCS-51)** - **[8085](https://en.wikipedia.org/wiki/Intel_8085)**
  * **RCA**: **[1802](https://en.wikipedia.org/wiki/RCA_1802)**
  * **Zilog**: **[ Z80](https://en.wikipedia.org/wiki/Zilog_Z80)**
* Upcoming microprocessors:  
  * Fairchild: F8
  * Intel: 4004, 4040, 8008, 8080, 8088, MCS-48 (the whole line-up :))
  * Motorola: 6800, 6801/6803, 68008
  * National: SC/MP, SC/MP II
  * Signetics: 2650, 8X300
  * Texas Instruments TMS9900
  * Commodore SID
* Hardware Specs:
    * Microprocessor runs at about 100~170kHz.
    * RAM : ~6 KB (available in Arduino Mega, expandable to 128 KB via expansion)
    * ROM : >200 KB (available in Arduino Mega)
    * Peripherals: UART, PIA, Timers, etc. (emulated by Arduino)
* Project goal is to make a simple platform to keep microprocessors alive
  * No need to build complex circuits, or burn EEPROMs :)
  * If you have the chip, you can bread-board it in one night.
  * Existing Arduino shields can be used to extend the capabilities, i.e. SDCard, LCD.
  * You can learn how microprocessors interface w/ memory or peripherals.
  * Run some of the great older software avaliable.
  * PCB/Kits are available for [sale](http://www.8bitforce.com/store/) (your purchase helps support more microprocessors)
* Open source hardware/software.

### Status

2019/08/27: C64 and Teensy.

* Michael Steil and I brought up Commodore 64 BASIC running on RetroShield6502. Blue screen and full cursor keys supported.
* I brought up RetroShield6502 on Teensy 3.5 boards (cpu runs at full speed 1.0+Mhz, 256K RAM, SDCard, 5V tolerant). Requires an interface board, which is my next high priority project. 
* At VCFWest, we got Monster6502 running on RetroShield6502, www.monster6502.com.
* Started wiring 2650 but got side-tracked by Teensy3.5.

Summary of where we are:

| Microprocessor | Emulated Hardware | Software |
|----------------|-------------------|----------|
| 6502 / W65C02S | Apple-1 (6502+6521)<br>KIM-1 (6502 + 6530) | MON,BASIC,Cassette (Steve W)<br>KIM-1 (MOS Technology, Inc) |
| 6809           | 6809E + FTDI        | Simon6809 (Lennart Benschop)|
| Z80            | Z80 + 8251          | Microsoft Basic (MSFT, Grant Searle)<br>EFEX (Mustafa Peker) |
| 8031           | 8031 + UART         | PAULMON2 (Paul Stoffregen) |
| 8085           | 8085A + 8251        | MON85 (David Dunfield) |
| 1802           | 1802 + UART         | MCSMP (Charles J, Yakym)<br>Tiny BASIC (Tom Pittman)<br>RCA BASIC3 (Ron Cenker) |
| Signetics 2650 | next | |
| Intel 8080     | next | |
| Motorola 6801 / 6803 | next | |
| Motorola 68008 | next | |

### Arduino Code

Links to Arduino Code for each microprocessor:

* [RetroShield 6502 Software](https://gitlab.com/8bitforce/retroshield6502)
    * **Apple I** Monitor + BASIC + Cassette Interface
    * **KIM-1** thru TTY
    * RetroShield 6502 even works with [Monster 6502](https://monster6502.com/)
* [RetroShield 6809 Software](https://gitlab.com/8bitforce/retroshield6809)
    * **Simon6809**, educational monitor w/ assembler/disassembler
* [RetroShield Z80 Software](https://gitlab.com/8bitforce/retroshieldz80)
    * **Z80 Basic Ver 4.7b**, Microsoft BASIC, heavily modified by [Grant Searle](http://searle.hostei.com/grant/z80/SimpleZ80_32K.html).
    * Efex Monitor (work in progress, some functions do not work)
* [RetroShield 1802 Software](https://gitlab.com/8bitforce/retroshield1802)
    * **Membership Card Serial Monitor Program Ver. 1.5** by Charles J, Yakym.
    * **Tiny BASIC** by Tom Pittman Copyright 1982
    * **RCA Basic** Level 3 v1.1 by Ron Cenker, Copyrighted 1981
    * **IDIOT monitor** by [Lee Hart](http://www.sunrise-ev.com/membershipcard.htm)
* [RetroShield 8031 Software](https://gitlab.com/8bitforce/retroshield8031)
    * **PAULMON2 v2.1** by [Paul Stoffregen](https://www.pjrc.com/tech/8051/paulmon2.html)
* [RetroShield 8085 Software](https://gitlab.com/8bitforce/retroshield8085)
    * **MON85 Version 1.2** by Dave Dunfield and modified by Roman Borik.


### Chips at hand:

I secured the following chips and will get to them done next:

- Intel 4004
- Intel 4040
- Intel 8008
- Intel 8088
- National INS 8060
- Natioanl SC/MP II
- Signetics N8X300
- AMI S9900 (TMS 9900)
- Commodore SID

Working on obtaining:

- Motorola 6800
- Fairchild F8 or Mostek MK3850
- Harris HD6120 / Intersil 6100 / Harris HM-6100 (PDP-8)

Let me know if you want anything else to be added to the list.

### License Information

* Copyright 2019 Erturk Kocalar, 8Bitforce.com
* Hardware is released under [Creative Commons ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/).
* Software is released under the [MIT License](http://opensource.org/licenses/MIT).
* Distributed as-is; no warranty is given.
* See LICENSE.md for details.
* All text above must be included in any redistribution.


![65C02](./docs/images/the_force_65c02.jpg)

![6809E](./docs/images/the_force_6809e.jpg)

![Z80](./docs/images/the_force_z80.jpg)
