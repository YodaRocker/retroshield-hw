# RetroShield for Arduino Mega

### Boards

* `k65c02`: RetroShield for 6502 microprocessor
* `k6809e`: RetroShield for 6809E microprocessor
* `kz80`: RetroShield for Z80 microprocessor
* `k1802`: RetroShield for 1802 microprocessor
* `k8031`: RetroShield for 8031 microprocessor
* `k8085`: RetroShield for 8085 microprocessor

### Tools

* Hardware Design File Format is gEDA:
http://www.geda-project.org/

### Scripts

After installing gschem, pcb and tools, use makefile :

* `make sch`: open schematics
* `make pcb`: open pcb
* `make`: compile netlist for pcb
* `make clean`: clean up the intermediate files. 
* `make backup`: make a copy of sch + pcb in archive/ folder.
* `make gerber`: create gerber.zip for production

### Release Process

Once a design is released to manufacturing, copy the design
folder to /manufacturing/ folder with revision + date.
